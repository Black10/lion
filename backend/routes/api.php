<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'LoginController@login');
    //Route::post('addUser', 'UserController@create');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'LoginController@logout');
        Route::post('addUser', 'UserController@create');
        Route::post('editUser', 'UserController@update');
        Route::get('users', 'UserController@index');
        Route::post('deleteUser', 'UserController@delete');
        Route::get('facilities', 'FacilitiesController@index');
        Route::get('rooms', 'RoomsController@index');
        Route::post('addRoom', 'RoomsController@store');
        Route::post('editRoom', 'RoomsController@update');
        Route::post('deleteRoom', 'RoomsController@destroy');
        Route::post('addReservation', 'ReservationController@store');
        Route::get('reservations', 'ReservationController@index');
        Route::post('reservations', 'ReservationController@destroy');
    });
});
