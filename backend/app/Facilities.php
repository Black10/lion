<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facilities extends Model
{
    protected $fillable = [
        'name', 'icon'
    ];
    public function rooms()
    {
        return $this->belongsToMany('App\Rooms');
    }
}
