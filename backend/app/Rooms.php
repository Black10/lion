<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Rooms extends Model
{

    use Notifiable, HasApiTokens, SoftDeletes;

    protected $fillable = [
        'name', 'floor', 'address','size', 'seats', 'photo', 'level'
    ];
    public function facilities()
    {
        return $this->belongsToMany('App\Facilities');
    }
}
