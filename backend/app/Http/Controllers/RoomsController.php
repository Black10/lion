<?php

namespace App\Http\Controllers;

use App\Rooms;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class RoomsController extends Controller
{
    public function index()
    {

        try {
            $rooms = Rooms::orderBy('created_at', 'desc')->get();
            foreach ($rooms as $room) {
                $room->facilities;
            }
            return response()->json($rooms, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function store(Request $request)
    {
        $aws = env('APP_URL') . '/avatar.png';
        try {
            if ($request->file('file')) {
                $file = $request->file('file');
                $mimetype =  $file->getMimeType();
                $ext = $file->getClientOriginalExtension();
                $route = 'public';
                if (str_contains($mimetype, 'image')) {
                    $upl = Storage::disk('local')->putFile($route, new File($file), 'public');
                    $aws = env('APP_URL') . Storage::url($upl);
                }
            }
        } catch (\Throwable $th) {
            return response()->json($th, 202);
        }
        try {
            $room =  Rooms::create([
                'name' => $request->input('name'),
                'floor' => $request->input('floor'),
                'address' => $request->input('address'),
                'size' => $request->input('size'),
                'seats' => $request->input('seats'),
                'photo' => $aws,
            ]);
        } catch (\Throwable $th) {
            return response()->json($th, 202);
        }

        $facilities   = explode(",", $request->input('facilities'));
        $room->facilities()->sync($facilities);
        $room->facilities;
        return response()->json($room, 200);
    }

    public function update(Request $request)
    {
        $aws = env('APP_URL') . '/avatar.png';
        $room = Rooms::findOrFail($request->input('id'));

        $room->name = $request->input('name');
        $room->floor = $request->input('floor');
        $room->address = $request->input('address');
        $room->size = $request->input('size');
        $room->seats = $request->input('seats');
        try {
            if ($request->file('file')) {
                $file = $request->file('file');
                $mimetype =  $file->getMimeType();
                $ext = $file->getClientOriginalExtension();
                $route = 'public';
                if (str_contains($mimetype, 'image')) {
                    if ($room->photo !== env('APP_URL') . '/avatar.png') {
                        Storage::disk('local')->delete($room->photo);
                    }
                    $upl = Storage::disk('local')->putFile($route, new File($file), 'public');
                    $aws = env('APP_URL') . Storage::url($upl);
                    $room->photo = $aws;
                }
            }
        } catch (\Throwable $th) {
            return response()->json($th, 202);
        }

        $facilities   = explode(",", $request->input('facilities'));
        $room->facilities()->sync($facilities);
        $room->facilities;
        $room->save();
        return response()->json($room, 201);
    }

    public function destroy(Request $request)
    {
        Rooms::where('id', $request->input('id'))->delete();
        return $request->input('id');
    }
}
