<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Rooms;
use App\User;
use Illuminate\Http\Request;

class ReservationController extends Controller
{

    public function index()
    {
        try {
            $reservations = Reservation::orderBy('created_at', 'desc')->get();
            foreach ($reservations as $reservation) {
                $room = Rooms::find($reservation->room_id);
                $room->facilities;
                $reservation->room = $room;
                $user = User::find($reservation->user_id);
                $reservation->user = $user;
            }
            return response()->json($reservations, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function store(Request $request)
    {
        try {
            $reservation =  Reservation::create([
                'room_id' => $request->input('room_id'),
                'user_id' => $request->input('user_id'),
                'start' => $request->input('start'),
                'end' => $request->input('end'),
                'description' => $request->input('description'),
                'finished' => 0
            ]);
        } catch (\Throwable $th) {
            return response()->json($th, 202);
        }
        $room = Rooms::find($request->input('room_id'));
        $room->facilities;
        $reservation->room = $room;
        $user = User::find($request->input('user_id'));
        $reservation->user = $user;

        return response()->json($reservation, 200);
    }


    public function destroy(Request $request)
    {
        if ($request->input('type') == 'finish') {
            $reservation = Reservation::findOrFail($request->input('id'));
            $reservation->finished = 1;
            $reservation->save();

            $room = Rooms::find($reservation->room_id);
            $room->facilities;
            $reservation->room = $room;
            $user = User::find($reservation->user_id);
            $reservation->user = $user;
            return $reservation;
        } else {
            Reservation::where('id', $request->input('id'))->delete();
            return $request->input('id');
        }

    }
}
