<?php

namespace App\Http\Controllers;

use App\Facilities;
use Illuminate\Http\Request;

class FacilitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

        try {
            $facilities = Facilities::orderBy('created_at', 'desc')->get();
            return response()->json($facilities, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

}
