<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use App\User;
use Validator;

class LoginController extends Controller
{

    public function login(Request $request){

        $this->validate(request(), [
            'email'    => 'required',
            'pass'   => 'required',
        ]);
        $email = $request->input('email');
        $pass = $request->input('pass');
        $user = User::where('email', $email)->where('level', 'admin')->first();
        if ($user) {

            $getAuth = Http::withHeaders([
                'Content-Type' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest'
                ])->post(env('APP_URL').'/oauth/token', [
                    'client_id'     => env('PASSPORT_ID'),
                    'client_secret' => env('PASSPORT_SECRET'),
                    'grant_type'    => 'password',
                    'password'      => $pass,
                    'scope'         => '',
                    'username'      => $user->email
                ]);

            if ($getAuth->successful()) {

                return response()->json([
                    'success' => true,
                    'message' => 'login created successfully.',
                    'data'    => [
                        'token' => $getAuth->json(),
                        'user'  => $user
                    ]
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Wrong user verify data',
                    'data' => []
                ]);

            }
        }

        return response()->json([
            'success' => false,
            'message' => 'Unregistered user',
            'data' => []
        ]);

    }
    public function logout(){
        $user = auth()->user();
        $user->token()->revoke();
        return [
            'action'  => true,
            'message' => 'Your session has been terminated'
        ];
    }

}
