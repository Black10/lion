<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\User;
use Validator;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        try {
            $users = User::orderBy('created_at', 'desc')->get();
            return response()->json($users, 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function create(Request $request)
    {
        $validator  = Validator::make($request->all(), [
            'name'       => 'required',
            'email'       => 'required',
            'phone'       => 'required',
            'address'       => 'required',
            'level'       => 'required',
        ]);
        $aws = env('APP_URL').'/avatar.png';
        try {
            if ($request->file('file')) {
                $file = $request->file('file');
                $mimetype =  $file->getMimeType();
                $ext = $file->getClientOriginalExtension();
                $route = 'public';
                if (str_contains($mimetype, 'image')) {
                    $upl = Storage::disk('local')->putFile($route, new File($file), 'public');
                    $aws = env('APP_URL').Storage::url($upl);
                }
            }
        } catch (\Throwable $th) {
            return response()->json($th, 202);
        }
        $pass = '';
        if ($request->input('pass')) {
            $pass = bcrypt($request->input('pass'));
        } else {
            $pass = bcrypt('password');
        }
        try {

            $user =  User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $pass,
                'phone' => $request->input('phone'),
                'address' => $request->input('address'),
                'photo' => $aws,
                'level' => $request->input('level'),
            ]);

        } catch (\Throwable $th) {
            //throw $th;
            return response()->json($th, 202);
        }
        return response()->json($user, 200);
    }

    public function update(Request $request)
    {

        $validator  = Validator::make($request->all(), [
            'name'       => 'required',
            'email'       => 'required',
            'phone'       => 'required',
            'address'       => 'required',
            'level'       => 'required',
        ]);
        $user = User::findOrFail($request->input('id'));

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->address = $request->input('address');
        $user->level = $request->input('level');
        if ($request->input('pass')) {
            $user->password = bcrypt($request->input('pass'));
        }
        try {
            if ($request->file('file')) {
                $file = $request->file('file');
                $mimetype =  $file->getMimeType();
                $ext = $file->getClientOriginalExtension();
                $route = 'public';
                if (str_contains($mimetype, 'image')) {
                    if ($user->photo !== env('APP_URL').'/avatar.png') {
                        Storage::disk('local')->delete($user->photo);
                    }
                    $upl = Storage::disk('local')->putFile($route, new File($file), 'public');
                    $aws = env('APP_URL').Storage::url($upl);
                    $user->photo = $aws;
                }
            }
        } catch (\Throwable $th) {
            return response()->json($th, 202);
        }

        $user->save();
        return response()->json($user, 201);
    }
    public function delete(Request $request){
        User::where('id', $request->input('id'))->delete();
        return $request->input('id');
    }
}
