<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone');
            $table->string('address');
            $table->string('photo');
            $table->enum('level', ['admin', 'client'])->default('client');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
        DB::table("users")->insert([
            "name"=> "Lion Admin",
            "email"=> "recursoshumanos@lionintel.com",
            "password"=> bcrypt("password"),
            "phone"=> "449 454 0705",
            "address"=> "República de Perú 920-A, Jardines de Santa Elena, 20236 Aguascalientes, Ags.",
            "photo"=> "https://lh3.googleusercontent.com/cm/ABXenNnaVGcYHmRIj4Stdi33JcQBs4loUKQeRsLLTPVDhd45k95LCHHTmfW8KXrl7mLP=s70-p-k-rw-no",
            "level"=> "admin",
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
