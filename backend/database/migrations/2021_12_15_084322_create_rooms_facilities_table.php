<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities_rooms', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rooms_id')->unsigned();
            $table->bigInteger('facilities_id')->unsigned();
            $table->foreign('rooms_id')->references('id')->on('rooms');
            $table->foreign('facilities_id')->references('id')->on('facilities');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
