<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('icon');
            $table->timestamps();
        });

        DB::table("facilities")->insert([
            "icon"=> "mdi-wifi",
            "name"=> "Wifi",
        ]);
        DB::table("facilities")->insert([
        "icon"=> "mdi-air-conditioner",
        "name"=> "AC",

        ]);
        DB::table("facilities")->insert([
        "icon"=> "mdi-projector",
        "name"=> "Projector",

        ]);
        DB::table("facilities")->insert([
        "icon"=> "mdi-projector-screen-outline",
        "name"=> "White Board",

        ]);
        DB::table("facilities")->insert([
        "icon"=> "mdi-power-plug",
        "name"=> "Charging plug point",

        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
