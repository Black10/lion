import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";
import moment from "moment";

import axios from "axios";

Vue.use(Vuex);
const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
});

export default new Vuex.Store({
  state: {
    token: localStorage.getItem("user-token") || "",
    user: JSON.parse(localStorage.getItem("user")) || {},
    users: [],
    clients: [],
    facilities: [],
    rooms: [],
    reservations: [],
    roomSelected: null,
    dialogR: false,
    dialogB: false,
  },
  getters: {
    isAuthenticated: (state) => !!state.token,
  },
  mutations: {
    resetState(state) {
      (state.token = localStorage.getItem("user-token") || ""),
        (state.user = JSON.parse(localStorage.getItem("user")) || {}),
        (state.users = []),
        (state.facilities = []),
        (state.roomSelected = null),
        (state.rooms = []),
        (state.clients = []),
        (state.reservations = []);
    },
    finishReservation(state, {data, type}){
      if (type == "finish") {
        var index = state.reservations.findIndex(
          ({ id }) => id == data.data.id
        );
        if (index !== -1) {
          state.reservations.splice(index, 1, data.data);
        }
      } else {
        var indexD = state.reservations.findIndex(
          ({ id }) => id == data.data
        );
        if (indexD !== -1) {
          state.reservations.splice(indexD, 1);
        }
      }
    },
    finish(state) {
      state.reservations.map((res) => {
        if (res.finished == 0) {
          if (moment().isSameOrAfter(res.end)) {
            var post = {
              id: res.id,
              type: 'finish'
            };
            axios
              .post(
                `${process.env.VUE_APP_BASE_URL_API}/api/auth/reservations`,
                post,
                {
                  headers: {
                    Authorization: `Bearer ${state.token}`,
                    Accept: "application/json",
                  },
                }
              )
              .then((data) => {
                
                var index = state.reservations.findIndex(({ id }) => id == data.data.id);
                if (index !== -1) {
                  state.reservations.splice(index, 1, data.data);
                }
              })
              .catch((error) => {
                // handle error
                console.error(error);
              });
          }
        }
      });
    },
    setToken(state, { token, user }) {
      state.token = token;
      state.user = user;
    },
    setFacilities(state, facilities) {
      state.facilities = facilities;
    },
    setReservations(state, reservations) {
      state.reservations = reservations;
    },
    addReservationS(state, reservation) {
      state.reservations.unshift(reservation);
    },
    setRooms(state, rooms) {
      state.rooms = rooms;
    },
    setRoom(state, room) {
      state.roomSelected = room;
    },
    addRoomS(state, room) {
      state.rooms.unshift(room);
    },
    setUsers(state, users) {
      state.users = users;
      state.clients = [];
      users.map((user) => {
        if (user.level == "client") {
          state.clients.unshift(user);
        }
      });
    },
    addUserS(state, user) {
      state.users.unshift(user);
      if (user.level == "client") {
        state.clients.unshift(user);
      }
    },
    deleteUserS(state, user) {
      var index = state.users.findIndex(({ id }) => id == user);
      if (index !== -1) {
        state.users.splice(index, 1);
      }
    },
    deleteRoom(state, room) {
      var index = state.rooms.findIndex(({ id }) => id == room);
      if (index !== -1) {
        state.rooms.splice(index, 1);
      }
    },
    updateUsers(state, user) {
      var index = state.users.findIndex(({ id }) => id == user.id);
      if (index !== -1) {
        state.users.splice(index, 1, user);
      }
      if (user.id == state.user.id) {
        state.user = user;
      }
    },
    updateRoom(state, room) {
      var index = state.rooms.findIndex(({ id }) => id == room.id);
      if (index !== -1) {
        state.rooms.splice(index, 1, room);
      }
    },
    setDialogR(state) {
      state.dialogR = !state.dialogR;
    },
    setDialogB(state) {
      state.dialogB = !state.dialogB;
    },
  },
  actions: {},
  modules: {},
  plugins: [vuexLocal.plugin],
});
