import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import VueGooglePlaces from 'vue-google-places'
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'


Vue.use(VueGooglePlaces)
Vue.use(require('vue-moment'))
Vue.config.productionTip = false
const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674',
};

Vue.use(VueSweetalert2, options)


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
