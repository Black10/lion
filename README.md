## Lion

Sistema para para administrar salas de juntas, hecho con:

- Laravel 7
- Vue.js
- Vuetify.js

## Instalación.

- `git clone https://bitbucket.org/Black10/lion.git`

- Backend:
- `cd lion/backend`
- `composer install`
- `npm install`
- Cambia el archivo: `cp .env.example .env` y configura la base de datos.
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan passport:install`
- Agregar estas variables al .env y Copie el Client Secret con ID 2 que acaba de generar
- `PASSPORT_SECRET=xxxxxx ` 
- `PASSPORT_ID=2`
- `php artisan storage:link`
- Definir un VirtualHosts para el proyecto
- `configurar el .env APP_URL=virtual host`

- Frontend:
- `cd frontend`
- `npm install`
- Agregar un Archivo .env.local en frontend y agregar las siguientes variables
- `VUE_APP_BASE_URL_API=ruta con la que visualizan el backend`
- `VUE_APP_API_KEY_MAPS=AIzaSyC-t0XJizhCtc52_OlkT9wAl7IYGiu3LuY`
- `npm run serve`
- Ingresa a `http://localhost:8080`


inicie sesión.

- `email: recursoshumanos@lionintel.com` 
- `password: password`

Ejemplo:

<img src="/demo.png" alt="">